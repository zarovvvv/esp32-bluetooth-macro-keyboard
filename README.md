## esp32-bluetooth-macro-keyboard  
  
**Bluetooth macro keyboard with esp32 and 4x4 matrix keyboard.**  

- Use Arduino IDE for ESP-32 development tutorial - https://www.youtube.com/watch?v=wNtGHCrO7E4  
  
- Bluetooth library needed for the project - https://github.com/T-vK/ESP32-BLE-Keyboard check out the code examples as well.  
  
**Important:** Change "byte rowPins[ROWS]" and "byte colPins[COLS]" in the code to match your GPIO pins where keyboard is connected to. First 4 pins from left to right are for the rows.  **  
