#include <BleKeyboard.h>
#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns

char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'},
};

// Looking at the keypad from the front, the row pins are the first 4 from left to right.

byte rowPins[ROWS] = {02, 00, 04, 16}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {17, 05, 18, 19 }; //connect to the column pinouts of the keypad


Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// Set ble name, manifacturer and battery percentage appearance
BleKeyboard bleKeyboard("BLE_MacroKeyboard", "ESP-32", 100);

void setup() {
  Serial.begin(9600);
  bleKeyboard.begin();
}

// This will hold down all the following buttons + specified key.
void sendMacroCommand(uint8_t key) {
  bleKeyboard.press(KEY_LEFT_CTRL);
  bleKeyboard.press(KEY_LEFT_ALT);
  bleKeyboard.press(key);
}

void loop() {
  char key = keypad.getKey();

  // Only do something with the keypress if we
  // are connected to something via bluetooth
  
  if (bleKeyboard.isConnected() && key) {
   Serial.println(key);
    switch (key) {
      case '1':
        // Select all from vim-visual and copy to buffer
        bleKeyboard.press(KEY_ESC);
        bleKeyboard.print("G");
        bleKeyboard.print("V");
        bleKeyboard.print("gg");
        bleKeyboard.press(KEY_LEFT_CTRL);
        bleKeyboard.print("c");
        break;
      case '2':
        // VIM - copy whole line withouth newline
        bleKeyboard.press(KEY_ESC);
        bleKeyboard.print("0");
        bleKeyboard.print("v");
        bleKeyboard.print("g");
        bleKeyboard.print("_");
        bleKeyboard.print("y");
        break;
      case '3':
        // VIM - new line above the cursor (opozite to o)
        bleKeyboard.press(KEY_ESC);
        bleKeyboard.press(KEY_UP_ARROW);
        bleKeyboard.print("0");
        bleKeyboard.print("o");
        break;
      case '4':
        sendMacroCommand(KEY_F4);
        break;
      case '5':
        sendMacroCommand(KEY_F5);
        break;
      case '6':
        sendMacroCommand(KEY_F6);
        break;
      case '7':
        sendMacroCommand(KEY_F7);
        break;
      case '8':
        sendMacroCommand(KEY_F8);
        break;
      case '9':
        sendMacroCommand(KEY_F9);
        break;
      case '0':
        sendMacroCommand(KEY_F10);
        break;
      case '*':
        sendMacroCommand(KEY_F11);
        break;
      case '#':
        // this will print the documentation, nothing will be executed
        bleKeyboard.print("# delete branch locally");
        bleKeyboard.print("# git branch -d $branch");
        bleKeyboard.print("# delete branch remotely");
        bleKeyboard.print("# git push origin --delete $branch");
        break;
      case 'A':
        bleKeyboard.println("git status");
        break;
      case 'B':
        bleKeyboard.println("git add .");
        break;
      case 'C':
        bleKeyboard.print("git commit -m \"\"");
        // This moves the cursor between two "", it's a hexadecimal representation of left arrow, other keys documentation here - https://www.arduino.cc/en/Reference/KeyboardModifiers
        bleKeyboard.press(0xD8);
        break;
      case 'D':
        bleKeyboard.println("git push");
        break;
    }

    delay(100);
    bleKeyboard.releaseAll(); // this releases the buttons
  }
}
